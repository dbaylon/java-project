package com.java.sample.project;
/***
 * 
 * @author dbaylon
 *
 */
public class FunctionImplementation {
	
	public static void main(String[] args) {
		
		/** 
		 *  There could be 2 ways I can think to achieve the result.
		 *  
		 *  1. Recursion
		 *  2. Iteration
		 *  
		 *  The best way I could think for the solution is to use <b>2. Iteration </b>.
		 *  
		 *  Below would be my inputs:
		 *  a. Recursion can cause <i> Infinite Loop <i> which can cause a lot of issues if you're not careful.
		 *  b. Recursion always needs atleast one basis case to end eventually.
		 *  c. Recursion causes poor performance due to heavy push-pops every recursive calls
		 *  d. 
		 * 
		 */
		
		System.out.println(getValue(100));
	}
	
	
	/**
	 * @param input
	 * @return
	 */
	private static long getValue(long input) {
		/**
		 *  Implementation:
		 *  a. check the input. this is for special case of 0 and 1.
		 *  b. start a loop for the input value.
		 *  c. sum up the first value and second value.
		 *  d. let the second value be the first value for input.
		 *  e. let the sum be the second value for the next iteration.
		 *  
		 *  NOTE: This is also knows as the Fibonacci Sequence.
		 */
		
		
		if (input <= 1) {
			return input;
		}

		long firstValue = 0;
		long secondValue = 1;
		
		long result = 0;
		
		for(int i = 2; i <= input; i++){
			result = firstValue + secondValue;
			firstValue = secondValue;
			secondValue = result;
		}
		
		return result;
	}
}
